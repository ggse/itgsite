/*!
* Copyright (c) 2011, Justin Force and the Regents of the University of California
* Licensed under the BSD 3-Clause License
*/

jQuery.noConflict();
(function($) {

  // Need to cache init function in case it needs to be called again
  // (webkit/jQuery .ready() bug http://bugs.jquery.com/ticket/5521), and we
  // can't use arguments.callee because it's being deprecated.
  var init = function() {

    ////////////////////////////////////////////////////////////////
    // Cache some queries
    ////////////////////////////////////////////////////////////////
    var header = $('#header');
    var navigation = $('#navigation');
    var navigationUL = navigation.find('ul');
    var main = $('#main');
    var mainTable = $('#mainTable');
    var footer = $('#footer');
    var areaNavigation = $('#areaNavigation');
    var areaNavigationUL = areaNavigation.find('ul');


    ////////////////////////////////////////////////////////////////
    // Hide some thing to minimize FOUC impact
    ////////////////////////////////////////////////////////////////
    navigationUL.hide();
    areaNavigationUL.hide();


    ////////////////////////////////////////////////////////////////
    // Hack webkit .ready() behavior (fires too early)
    ////////////////////////////////////////////////////////////////
    if ($.browser.webkit && document.readyState != 'complete') {
      return setTimeout(init, 50);
    }


    ////////////////////////////////////////////////////////////////
    // Cache jQuerified window
    ////////////////////////////////////////////////////////////////
    var jWindow = $(window);


    ////////////////////////////////////////////////////////////////
    // Cache date
    ////////////////////////////////////////////////////////////////
    var date = new Date();


    ////////////////////////////////////////////////////////////////
    // Update copyright year
    ////////////////////////////////////////////////////////////////
    var copyrightYear = $('#copyrightYear');
    var fullYear = date.getFullYear();
    if (copyrightYear.text() != fullYear) {
      copyrightYear.append(' - ' + fullYear);
    }


    ////////////////////////////////////////////////////////////////
    // Set up navigation
    ////////////////////////////////////////////////////////////////
    navigationUL.show();
    navigation.addClass('jq').append('<br style="clear:both" />');
    var navLists = navigation.find('ul ul');

    jWindow.resize(function() {
      navLists.positionNavLists();
    });

    // Slide lists on hover. Hide them initially.
    navigation.find('li').hover(function() {
      $(this).find('ul').stop(true, true).slideToggle(100);
    }).find('ul').hide();

    // Highlight li on hover and pass clicks on the li through to its link
    navigation.find('li li').hover(function() {
      $(this).toggleClass('hovered');
    }).click(function() {
      location = $(this).find('a').attr('href');
    });

    // Position a navigation list relative to its parent and the edge of the
    // screen. If the right edge is off the screen, it should be nudged to the
    // left so that the right edge touches the screen.
    $.fn.positionNavLists = function() {
      return this.each(function() {
        // Cache a bunch of stuff
        var list = $(this);
        var outerWidth = list.outerWidth();
        var parent = list.parent();
        var parentPosition = parent.position();
        var windowWidth = jWindow.width();

        // Set the left edge of the list. Nudge it an extra pixel so the edge border is off screen
        var right = parentPosition.left + outerWidth;
        var left = right > windowWidth ? windowWidth - outerWidth + 1 : parentPosition.left - 1;

        list.css({
          top: parentPosition.top + parent.outerHeight(),
          left: left
        });
      });
    };


    ////////////////////////////////////////////////////////////////
    // Area navigation
    ////////////////////////////////////////////////////////////////
    // When an item with a child list is clicked, its list slides open and
    // closed. stopPropagation so that clicking children doesn't bubble up and
    // collapse the list.
    $('<img src=/img/plus.gif><img src=/img/minus.gif>'); // pre-load graphics
    areaNavigationUL.show(); // Must show before we start or positioning won't work
    areaNavigation.find('li:has(ul)').addClass('expandableList').click(function() {
      $(this).toggleClass('expandedList').children('ul').slideToggle('fast', function() {
        jWindow.resize(); // Because this can cause the layout to change
      });
    }).children('ul').hide().find('li').click(function(e) { e.stopPropagation(); });


    ////////////////////////////////////////////////////////////////
    // Set a minimum height for #main
    ////////////////////////////////////////////////////////////////
    // Set the height of #main so that the footer always reaches at least to
    // the bottom of the window. If main is already taller than the window,
    // don't set a height; let the page flow normally.
    jWindow.resize(function() {
      var windowHeight = jWindow.height();
      var newHeight = windowHeight - header.outerHeight() - navigation.outerHeight() - footer.outerHeight();
      newHeight = newHeight < windowHeight ? newHeight : null;
      (mainTable.size() > 0 ? mainTable : main).css({height: newHeight});
    });

    // Now that everything's set up, .resize() to trigger all those positioning
    // handlers.
    jWindow.resize(); 
  };

  $(init);

})(jQuery);
